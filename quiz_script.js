var nyelv=1;

function check(kerdesek){
	var output=document.getElementById("eredmeny");
	var hibas=0;
	var where=[];
	document.getElementById("eredmeny").innerHTML="";
	if(nyelv==0){
	for(var i=0;i<kerdesek;i++){
		var which=document.getElementsByClassName(i);
		var attribute=which[0].getAttribute("type");
		if(attribute=="radio"){
			var j=0;
			while(!which[j].checked){
					j++;
				}
				if(which[j].value=="false")
				{
					where[hibas]=i+1;
					hibas++;
				}
			}
		else if(attribute=="checkbox"){
			var correct=Number(document.getElementById(i+"kerdes").getAttribute("value"));
			var jo=0;
			var rossz=0;
			for(var j=0;j<which.length;j++){
				if(which[j].value=="true"&&which[j].checked){
					jo++;
				}
				else if(which[j].value=="false"&&which[j].checked){
					rossz++;
				}
			}
			if(jo!=correct){
				where[hibas]=i+1;
				hibas++;
			}
			else if(rossz!=0){
				where[hibas]=i+1;
				hibas++;
			}
		}
	
	}
	if(hibas==0){
	document.getElementById("hun").style.display="none";
	document.getElementById("reward").style.display="initial";
	output.innerHTML="Gratulálok minden válasz helyes volt!";
	document.getElementById("delete").style.width="79%";
	var hide=document.getElementsByClassName("rejtendo");
	for(var i=0;i<hide.length;i++){
		hide[i].style.display="none";
	}
	}
	else{
		output.innerHTML=hibas+" helytelen válasz volt.<br> A(z) ";
		for(var i=0;i<hibas;i++){
		output.innerHTML+=where[i]+". ";
		}
		output.innerHTML+="kérdés.";
	}
	}
	if(nyelv==1){
	for(var i=10;i<kerdesek*2;i++){
		var which=document.getElementsByClassName(i);
		var attribute=which[0].getAttribute("type");
		if(attribute=="radio"){
			var j=0;
			while(!which[j].checked){
					j++;
				}
				if(which[j].value=="false")
				{
					where[hibas]=i+1;
					hibas++;
				}
			}
		else if(attribute=="checkbox"){
			var correct=Number(document.getElementById(i+"kerdes").getAttribute("value"));
			var jo=0;
			var rossz=0;
			for(var j=0;j<which.length;j++){
				if(which[j].value=="true"&&which[j].checked){
					jo++;
				}
				else if(which[j].value=="false"&&which[j].checked){
					rossz++;
				}
			}
			if(jo!=correct){
				where[hibas]=i+1;
				hibas++;
			}
			else if(rossz!=0){
				where[hibas]=i+1;
				hibas++;
			}
		}
	
	}
	if(hibas==0){
	document.getElementById("eng").style.display="none";
	document.getElementById("reward").style.display="initial";
	output.innerHTML="Congratulations all of your answers were right!";
	document.getElementById("delete").style.width="79%";
	var hide=document.getElementsByClassName("rejtendo");
	for(var i=0;i<hide.length;i++){
		hide[i].style.display="none";
	}
	}
	else{
		output.innerHTML=hibas+" mistakes have been made.<br> The ";
		for(var i=0;i<hibas;i++){
		output.innerHTML+=where[i]-10+". ";
		}
		output.innerHTML+="qustion.";
	}
	}
}
function change (){
	var output=document.getElementById("eredmeny");
	if(nyelv==0){
		document.getElementById("reward").style.display="none";
		output.innerHTML="";
		nyelv++;
		document.getElementById("eng").style.display="initial";
		document.getElementById("hun").style.display="none";
		document.getElementById("allito").style.backgroundImage='url("engpic.png")';
	}
	else{
		document.getElementById("reward").style.display="none";
		nyelv--;
		output.innerHTML="";
		document.getElementById("hun").style.display="initial";
		document.getElementById("eng").style.display="none";
		document.getElementById("allito").style.backgroundImage='url("hunpic.png")';
	}
}